

public class Consultant 
{
    protected const int LENGTH_NUMBER_PHONE = 11;
    protected const char CODE_COUNTRY = '7';
    private string maskData = "**********";
    public string Surname { get; set; }
    public string Name { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public string Pasport { get; set; }
    protected string path = "Data.txt";
    public Consultant()
    {
        Surname = string.Empty;
        Name = string.Empty;
        LastName = string.Empty;
        PhoneNumber = string.Empty;
        Pasport = string.Empty;
    }

    public void OutputInfo()
    {
        foreach(string line in File.ReadLines(path))
        {
            var data = line.Split(':').ToList();
            data[0] = GetPaspotr(data[0]);
            Console.WriteLine($"{data[0]}:{data[1]}:{data[2]}");
        }
    }

    public void ChangePhoneNumber()
    {
        Console.WriteLine("Введите номер, который хотите изменить:");
        string? oldNumber = Console.ReadLine();

        Console.WriteLine("Введите новый номер:");
        string? newNumber = Console.ReadLine(); 

        if((!String.IsNullOrWhiteSpace(newNumber) && !String.IsNullOrWhiteSpace(oldNumber)) 
        && CheckCorrectNumber(newNumber))
        {
            string text = File.ReadAllText(path);
            text = text.Replace(oldNumber, newNumber);
            File.WriteAllText(path, text);
            Console.WriteLine("Номер успешно изменен!");
        }
        else
            Console.WriteLine("Номер введен некорректно! Повторите попытку ввода.");
    }

    private bool CheckCorrectNumber(string number)
    {
        return number.All(Char.IsDigit) && number.Length == LENGTH_NUMBER_PHONE && number.StartsWith(CODE_COUNTRY);
    }

    private string GetPaspotr(string pasport)
    {
        if(String.IsNullOrWhiteSpace(pasport))
            return "Данных нет";
        return maskData;
    }
}
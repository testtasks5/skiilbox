﻿// 222233333330:Sidorov-Pavel-Vasilvich:79010000000 Example data
Console.WriteLine("Hello, World!");
Consultant tc = new Consultant() {
    Surname = "Sidorov",
    Name = "Pavel",
    LastName = "Vasilvich",
    PhoneNumber = "79010000000",
    Pasport = "222233333330",
};
Dictionary<string, Delegate> comandDict = new Dictionary<string, Delegate>()
{
    {"info", tc.OutputInfo},
    {"edit", tc.ChangePhoneNumber}
};

Console.WriteLine("Список команд:");
Console.WriteLine("0 - выход из программы.");
Console.WriteLine("info - Вывод данных о пользователе.");
Console.WriteLine("edit - Изменение номера телефона пользователя.");

string? enter = string.Empty;
Console.WriteLine("Введите команду:");
while(enter != "0")
{   
    enter = Console.ReadLine();
    var action = comandDict.FirstOrDefault(c => c.Key == enter);
    if(!String.IsNullOrWhiteSpace(action.Key))
        comandDict[action.Key].DynamicInvoke();

    Console.WriteLine("Введите команду или 0 для выхода.");
}